<?php

//home
Route::get('/home', '\App\Http\Controllers\HomeController@index')->name('home');;

//login
Route::get('/', '\App\Http\Controllers\Auth\LoginController@showLoginForm')->name('login');
//Route::get('login', '\App\Http\Controllers\Auth\LoginController@showLoginForm')->name('login');
Route::post('/', '\App\Http\Controllers\Auth\LoginController@login');

//logout
Route::post('logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');

//registration
Route::get('register', '\App\Http\Controllers\Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', '\App\Http\Controllers\Auth\RegisterController@register');

//resetpass
Route::get('password/reset', '\App\Http\Controllers\Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', '\App\Http\Controllers\Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', '\App\Http\Controllers\Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', '\App\Http\Controllers\Auth\ResetPasswordController@reset')->name('password.update');

//confirmpass
Route::get('password/confirm', '\App\Http\Controllers\Auth\ConfirmPasswordController@showConfirmForm')->name('password.confirm');
Route::post('password/confirm', '\App\Http\Controllers\Auth\ConfirmPasswordController@confirm');

//emailverify
Route::get('email/verify', '\App\Http\Controllers\Auth\VerificationController@show')->name('verification.notice');
Route::get('email/verify/{id}/{hash}', '\App\Http\Controllers\Auth\VerificationController@verify')->name('verification.verify');
Route::post('email/resend', '\App\Http\Controllers\Auth\VerificationController@resend')->name('verification.resend');


Route::group(['middleware' => ['role:admin']], function () {
    //users
    Route::resource('users', '\App\Http\Controllers\UserController');

    //event calendar
    Route::resource('calendar', '\App\Http\Controllers\EventController');
});
