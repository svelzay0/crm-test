<?php 

namespace App\Filters;
use JeroenNoten\LaravelAdminLte\Menu\Filters\FilterInterface;

class RoleFilter implements FilterInterface
{
    public function transform($item)
    {
        if (isset($item['role']) && auth()->check() && ! auth()->user()->hasRole($item['role'])) {
            $item['restricted'] = true;
        }
        return $item;
    }
}