<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EventController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('calendar.index');
    }  
    public function create()
    {   

    }
    public function store(ArticleRequest $request)
    {   

    }
    public function show(Article $article)
    {   

    }
    public function edit($id)
    {   

    }
    public function update(Article $article, ArticleRequest $request)
    {    
        // $article->update($request->all());
        // $this->syncTags($article, $request->input('tag_list'));
        // return redirect('articles')->with('status', 'Статья ' .$article->title. ' успешно изменена.');
    }
    public function destroy($id)
    {   
        // $article = Article::findOrFail($id);
        // $article->delete($id);
        // return redirect('articles')->with('status', 'Статья ' .$article->title. ' успешно удалена.');
    }
   
}
