<?php

namespace App\Http\Controllers;

use Yajra\DataTables\Html\Column;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use App\DataTables\UserDataTable;
use Yajra\DataTables\Html\Builder; // import class on controller
use App\Models\User;
use App\Models\Role;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UserEditRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Validator;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request, Builder $htmlBuilder)
    {
        if ($request->ajax()) {
            return Datatables::of(User::select(['id', 'name', 'email', 'created_at', 'updated_at']))

            ->addColumn('action', function($user) {
                return view('users.buttons.action', ['user' => $user]);
            } )           
            ->rawColumns(['view', 'edit', 'delete'])
            ->make(true);
        }
        
        $html = $htmlBuilder
            ->addColumn(['data' => 'id', 'name' => 'id', 'title' => 'Номер'])
            ->addColumn(['data' => 'name', 'name' => 'name', 'title' => 'Имя'])
            ->addColumn(['data' => 'email', 'name' => 'email', 'title' => 'Почта'])
            ->addColumn(['data' => 'created_at', 'name' => 'created_at', 'title' => 'Создан'])
            ->addColumn(['data' => 'updated_at', 'name' => 'updated_at', 'title' => 'Обновлен'])
            ->addColumn(['data' => 'action', 'name' => 'action', 'title' => 'Функции']);
        
        $html = $html->parameters([
            'language' => [
                'url' => url('js/langDatatables.json') //localization html-builder
            ],
        ]); 
   
        return view('users.index', compact('html'));
    }
    public function show(User $user, Role $role)
    {   
        return view('users.show', compact('user','role'));
    }
    public function create(User $user)
    {   
        $role = Role::pluck('name','id');
        $roleId = $user->roles->pluck('id');
        return view('users.create', compact('user', 'role', 'roleId'));
    }
    public function store(User $user, UserRequest $request)
    {           
        $user = $user->create($request->all());             
        $user->attachRole($request->role);
        flash(__('flash-messages.create-success'))->success();
        return redirect('users');
    }
    public function edit(User $user)
    {   
        $role = Role::pluck('name','id');
        $roleId = $user->roles->pluck('id');
        return view('users.edit', compact('user', 'role', 'roleId'));
    }
    public function update(User $user, UserEditRequest $editRequest)
    {          
        $editRequestAll = $editRequest->all();
        if(!$editRequestAll['password']) {
            unset($editRequestAll['password']);
        }
        $user->update($editRequestAll);     
        $user->roles()->sync($editRequest->role);         
        flash(__('flash-messages.update-success'))->success();
        return redirect('users');
    }
    public function destroy (User $user) 
    {
        $user->delete($user->id);
        flash(__('flash-messages.del-success'))->success();
        return ['status' => 'success'];
    }
}
