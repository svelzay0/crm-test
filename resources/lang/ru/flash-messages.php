<?php

use App\Models\User;
$user = new user();

return [

    'del-success' => 'Пользователь успешно удален',
    'del-error'     => 'Пользователь не удален!',
    'create-success' => 'Пользователь'. $user->name. ' успешно создан.',
    'update-success' => 'Пользователь'. $user->name. ' успешно изменен.'
    
];
