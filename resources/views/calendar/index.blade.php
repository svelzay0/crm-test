@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Календарь</h1>
@stop

@section('css')
    <link href="{{ asset('fullcalendar/lib/main.css') }}" rel='stylesheet' />
    <!-- <link href='https://cdn.jsdelivr.net/npm/bootstrap@4.5.0/dist/css/bootstrap.css' rel='stylesheet' /> -->
    <link href='https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.13.1/css/all.css' rel='stylesheet'>
@stop

@section('content')



    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <p class="mb-0">   
                    <div class="input-group">  
                        <a class="btn btn-primary" href="{{route('users.create')}}">
                            Создать событие
                        </a>                       
                    </div>  
                    <hr/>
                    <div id='calendar'></div>         
                                                                                      
                </p>
            </div>
        </div>
    </div>


@endsection

@section('js')

{{-- FullCalendar --}}

    <script src="{{ asset('fullcalendar/lib/main.js') }}"></script>
    <script src="{{ asset('fullcalendar/lib/locales-all.js') }}"></script>
    <script>
      document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar');
        var calendar = new FullCalendar.Calendar(calendarEl, {
            themeSystem: 'bootstrap'
        });       
        calendar.render();
        calendar.setOption('height', 700);
        calendar.setOption('contentHeight', 650);
        calendar.setOption('locale', 'ru');
      });
    </script>

@stop