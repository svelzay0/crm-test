@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Информация: {{$user->name}}</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body"> 
                    <div class="col-md-12">
                        <div class="card card-primary">
                            <div class="card-header">                           
                                <h3 class="card-title">Данные пользователя</h3>
                            </div>
                        </div>    
                        <div class="card-footer">     
                            <div class='d-flex'>                
                                <div>                    
                                    <span>
                                        <b>name:  </b>
                                    </span> 
                                    <span class="p-2 badge-light">
                                        {{$user->name}}
                                    </span>                                                    
                                                      
                                    <span>
                                        <b>email:  </b>
                                    </span>   
                                    <span class="p-2 badge-light">
                                        {{$user->email}}
                                    </span>                                                    
                  
                                    <span>
                                        <b>password:  </b>
                                    </span>   
                                    <span class="p-2 badge-light">
                                        {{$user->password}}
                                    </span>                                                    
                  
                                    <span>
                                        <b>created at:  </b>
                                    </span>   
                                    <span class="p-2 badge-light">
                                        {{$user->created_at}}
                                    </span>                                                    
                   
                                    <span>
                                        <b>updated at:  </b>
                                    </span>   

                                    <span class="p-2 badge-light">
                                        {{$user->updated_at}}
                                    </span>                                                    
                                </div>                               
                            </div>
                        </div>
                        <hr/>                   
                        <div class="d-flex">
                            <div class="p-2">
                                <a class="btn btn-info" href="{{route('users.index')}}">Назад</a>
                            </div>
                            <div class="p-2">
                                <a class="btn btn-warning" href="{{route('users.edit', $user->id)}}">Редактировать</a>     
                            </div>                                  
                        </div>                                                                  
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



    