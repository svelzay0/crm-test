@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Редактировать: {{$user->name}}</h1>
@stop

@section('content')

  <form action="{{route('users.update',$user->id)}}" method="POST"> 
    @csrf
    @method('PATCH')
    @include('users.form',['submitButton'=>'Изменить'])
  </form>

  <hr/>                   
  <div class="d-flex">
    <div class="p-2">
        <a class="btn btn-info" href="{{route('users.index')}}">Назад</a>
    </div>                                 
  </div>
  
@endsection
