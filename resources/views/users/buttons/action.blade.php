<div class="d-flex">
<div class="p-2"> 
    <a class="btn btn-info" href="{{route('users.show', $user->id)}}">Посмотреть</a>    
  </div>
  <div class="p-2">    
    <a class="btn btn-warning" href="{{route('users.edit', $user->id)}}">Редактировать</a>       
  </div>
  <div class="p-2"> 
    @csrf
    @method('DELETE')
    <button class="btn btn-danger" type='button' onclick="deleteUser(`{{ route('users.destroy', $user->id) }}`, 'DELETE')">Удалить</button>     
  </div>
</div>


