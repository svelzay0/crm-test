<div class="card card-primary">
  <div class="card-header">
    <h3 class="card-title">Введите данные пользователя</h3>
  </div>
  <form id="quickForm">
    <div class="card-body">
      <div class="form-group">
        <label for="exampleInputName1">
          Имя
        </label>   
        <input type="name" name="name" class="form-control {{ ($errors->has('name')) ? 'is-invalid' : '' }}" id="exampleInputName1" value="{{ old('name') ?? $user->name ?? '' }}" placeholder="Jeff">
          @error('name')
            <div class="badge text-danger">
              {{ $message }}
            </div>
          @enderror      
      </div>  
      <div class="form-group">
        <label for="exampleInputEmail1">
          Почта
        </label>     
        <input type="email" name="email" class="form-control {{ ($errors->has('email')) ? 'is-invalid' : '' }}" id="exampleInputEmail1" value="{{ old('email') ?? $user->email ?? '' }}" placeholder="asd@gmail.com">
          @error('email')
            <div class="badge text-danger">
              {{ $message }}
            </div>
          @enderror 
      </div>              
      <div class="form-group">
        <label for="exampleInputPassword1">
          Пароль
        </label>
        <input type="password" name="password" class="form-control {{ ($errors->has('password')) ? 'is-invalid' : '' }}" id="exampleInputPassword1" value="" placeholder="123456">
          @error('password')
            <div class="badge text-danger">
              {{ $message }}
            </div>
          @enderror         
      </div>  
      <div class="form-group">
        <label for="exampleInputPassword1">
          Роль
        </label>
        <select class="form-control" name="role">
          @foreach ($role as $id => $name)                         
            <option class="input-group p-2 justify-content-center" value="{{$id}}" {{ ($roleId->contains($id)) ? 'selected' : '' }}>
              {{$name}}
            </option>
          @endforeach
        </select>      
      </div>       
    </div>       
    <div class="card-footer">
      <button type="submit" class="btn btn-primary">{{$submitButton}}</button>
    </div>      
  </form>
</div>

  