@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1 class="m-0 text-dark">Пользователи</h1>
@stop

@section('content')
@if (session('status'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{ session('status') }}
        @include('flash::message')
    </div>                  
@endif

    <div id='del'>
        @include('flash::message')
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <p class="mb-0"> 
                        <div class="input-group">                        
                            <a class="btn btn-primary" href="{{route('users.create')}}">
                                Создать пользователя
                            </a>                         
                        </div>                         
                        <br>
                        {!! $html->table() !!}                   
                    </p>
                </div>
            </div>
        </div>
    </div>
    
@endsection

@section('js')
    <script>
        function removeElementsByClass(className){
            var elements = document.getElementsByClassName(className);
            while(elements.length > 0){
                elements[0].parentNode.removeChild(elements[0]);
            }
        }
        function deleteUser(route, method) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: method, // метод HTTP, используемый для запроса
                url: route, // строка, содержащая URL адрес, на который отправляется запрос
                success: function(data) {
                    LaravelDataTables.dataTableBuilder.ajax.reload();
                    let parent = $("#del");
                    let row = document.createElement("div");
                    row.className = "alert alert-success";
                    parent.append(row)
                    row.append("{{__('flash-messages.del-success')}}");
                    $(".alert").fadeTo(2000, 500).slideUp(500, function() {
                        $(".alert").slideUp(500).delay(3000);
                        $('.alert-success').remove();
                        });
                },
                error: function(data) {
                    alert("{{__('flash-messages.del-error')}}");
                },
            });
        }             
    </script>
    {!! $html->scripts() !!}
@endsection
